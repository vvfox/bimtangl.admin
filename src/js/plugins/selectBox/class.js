
import PluginBase from '../pluginBase';
import Discloser from '../discloser/class';
import defaults from './defaults';

export default class SelectBox extends PluginBase {

	constructor(element, options) {
		super(element, options, defaults);
		this.currentValue = undefined;
		this.init();
	}

	init() {
		let self = this;
		this.setStructure();
		if(this.options.isClosable) {
			this.discloser = new Discloser(this.element, this.options.discloserOptions);
			$('body').on('click', () => {
				this.discloser.close();
			});
			this.$element.on('click', e => {
				e.stopPropagation();
			});
		}
		this.$option.on('click', function () {
			self.selectOption($(this));
		});
	}

	selectOption($option) {
		const value = $option.val() || $option.data('value');

		if(value) {
			this.$notice.addClass(this.options.hiddenClass);
			this.$caption.removeClass(this.options.hiddenClass);
			this.$units.removeClass(this.options.hiddenClass);
		} else {
			this.$notice.removeClass(this.options.hiddenClass);
			this.$caption.addClass(this.options.hiddenClass);
			this.$units.addClass(this.options.hiddenClass);
		}

		if(value !== this.currentValue) {
			$option.prop('checked', true);
			this.currentValue = value;
			this.$caption.html(value);
			$option.trigger(this.options.changeEvent, [$option.attr('name'), value]);
		}

		if (this.discloser) { this.discloser.close(); }

	}

	selectOptionByIndex(index) {
		this.selectOption(this.$option.eq(index))
	}

	selectOptionByValue(value) {
		this.selectOption(this.$option.filter((i, el) => {
			return $(el).val() == value;
		}));
	}

	enableOptionsByValues(values) {
		const $options = values ? this.$option.filter((i, el) => {
			return values.indexOf($(el).val()) !== -1;
		}) : this.$option;
		this.$option.addClass(this.options.disabledClass);
		$options.removeClass(this.options.disabledClass);
	}

}