export default  {
	structure: {
		caption: '.caption',
		option: '.option',
		units: '.units',
		notice: '.notice',
	},
	activeClass: window.classes.active,
	disabledClass: window.classes.disabled,
	hiddenClass: window.classes.hidden,
	isClosable: true,
	changeEvent: 'selectbox.change',
	discloserOptions: {
		slide: true
	}
};