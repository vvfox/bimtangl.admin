
import Discloser from './class';

const pluginName = 'discloser';

$.fn[pluginName] = function (options, ...params) {
	Discloser.registerPlugin(this, pluginName, Discloser, options, ...params)
};

$.fn[pluginName].Constructor = Discloser;