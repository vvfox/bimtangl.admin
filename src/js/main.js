import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import app from '@/components/global/page.vue';

import {router} from './router';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';
// oidc 


// import { createOidcAuth, SignInType } from 'vue-oidc-client';

// // note the ending '/'
// const appUrl = '/', authName = 'main';

// // SignInType could be Window or Popup
// let mainOidc = createOidcAuth(authName, SignInType.Window, appUrl , {
 
//     authority: "https://auth.st.tangl.cloud/api/",
//     client_id: "TanglAccount_Web",
//     // redirect_uri: "/callback.html",
//     response_type: "code",
//     scope:"openid email profile role IdentityServerApi",
//     // post_logout_redirect_uri: "/index.html"
// });

//pages 






const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);
Vue.use(VueRouter);
Vue.mixin({
    created: function () {
      
    },
    methods: {
      format_date: function (s_date, show_time = false) {
        var d_date = new Date(s_date);
                var dd = d_date.getDate();
                if (dd < 10) dd = '0' + dd;

                var mm = d_date.getMonth() + 1;
                if (mm < 10) mm = '0' + mm;

                var yy = d_date.getFullYear() ;
                if (yy < 10) yy = '0' + yy;

                return dd + '.' + mm + '.' + yy + (show_time? [' ' , 
                    (d_date.getHours()<10?'0':'') , d_date.getHours() , ':' , 
                    (d_date.getMinutes()<10?'0':'') , d_date.getMinutes()].join(''):'');
      }
    }
  });

  


export default function(){
    (function renderApp() {
      
        let appVue = new Vue({
            el: '#app',
            render: h => h(app),
            store,
            router
         });
    }());

}
