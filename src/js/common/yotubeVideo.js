(function youTubeVideo(global, document, $, undefined) {

	function YouTubeVideo(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	YouTubeVideo.prototype.defaults = {

		structure: {
			videoWrapper: '.video-over',
			player: '.player',
			play: '.btn-play',
			fullScreen: '.control--full-screen',
			timeline: '.b-timeline'
		},
		ytOptions: {
			width: 640,
			height: 360,
			playerVars: {
				controls: 0,
				enablejsapi: 1,
				origin: 'http://localhost:3000',
				showinfo: 0,
				modestbranding: 1,
				rel: 0
			},
			events: {

			}
		},
		autoLoad: true,
		playInFullSize: true,
		popup: '.b-popup--video',
		onStop: function () {
			$.magnificPopup.close();
		}

	};

	function init() {

		var self = this;

		self.$window = $(window);
		self.playerState = 'stop';
		self.init = false;
		$.proxy(setStructure, self)();
		self.$popup = $(self.options.popup);
		$.proxy(setPlayerId, self)();
		$.proxy(setVideoId, self)();

		self.$window.on(window.events.ytReady, $.proxy(onApiReady, self));

		self.$play.on('click', $.proxy(onPlay, self));

		self.$fullScreen.on('click', function () {
			if(!window.isFullScreen()) {
				window.enterFullScreen(self.$element.get(0));
			} else {
				window.exitFullScreen();
			}

			return false;

		});

	}

	function setStructure() {

		var self = this,
				structure = self.options.structure;

		$.each(structure, function (name, value) {

			self['$' + name] = self.$element.find(value);

		});

	}

	function setPlayerId() {

		this.playerId = this.$player.attr('id');

	}

	function setVideoId() {

		this.videoId = this.$player.data('video');

	}

	function onApiReady() {

		var self = this;

		if(this.options.autoLoad)
			$.proxy(loadPlayer, self)();

	}

	function loadPlayer() {

		var self = this,
				ytOptions = self.options.ytOptions;

		ytOptions.videoId = self.videoId;
		ytOptions.events.onReady = $.proxy(onPlayerReady, self);
		ytOptions.events.onStateChange = $.proxy(onStateChange, self);
		self.player = new YT.Player(self.playerId, ytOptions);

	}

	function onPlayerReady() {

		var self = this;

		self.playerReady = true;
		$.proxy(setStructure, self)();
		self.$window.trigger('playerReady');

	}

	function onPlay() {

		var self = this;

		if(self.options.playInFullSize)
			$.proxy(openFullScreen, self)();
		else
			$.proxy(playVideo, self)();


		return false;

	}

	function playVideo() {

		var self = this,
				player = self.player,
				playerState = self.playerState;

		if(self.$timeline.length) {
			self.$timeline.timeline({
				$video: self.$element,
				playEvent: 'videoPlaying',
				getDuration: function () {
					return player.getDuration();
				},
				getCurrentTime: function () {
					return player.getCurrentTime();
				},
				setVideoTime: function (curTime) {
					player.seekTo(curTime, true);
				},
				onStop: self.options.onStop
			});
		}

		if(!self.init) {
			self.init = true;
			self.$element.trigger('initialized');
		}

		if(playerState === 'stop' || playerState === 'pause') {
			self.playVideo();
		} else {
			self.pauseVideo();
		}

	}

	function openFullScreen() {

		var self = this,
				$videoPopup = self.$popup,
				$player = self.$player,
				play = $.proxy(playVideo, self);

		$videoPopup.append($player);

		$.magnificPopup.open({

			items: {
				src: $videoPopup,
				type: 'inline'
			},
			mainClass: 'mfp-custom mfp-full-size mfp-video',
			fixedContentPos: true,
			callbacks: {
				open: function () {

					self.$window.on('playerReady', play);

					window.enterFullScreen($videoPopup.get(0));


				},
				close: function () {

					self.$window.off('playerReady', play);
					self.$videoWrapper.append($player);
					self.$timeline.data('timeline').destroy();
				}
			}

		});
	}

	function onStateChange(event) {

		var self = this,
				statusCode = event.data;

		if(statusCode === YT.PlayerState.ENDED) {
			self.stopVideo();
		}
	}

	YouTubeVideo.prototype.loadPlayer = function () {

		$.proxy(loadPlayer, this)();

	};

	YouTubeVideo.prototype.playVideo = function () {
		this.player.playVideo();
		this.$element.trigger('videoPlaying');
		this.playerState = 'play';
	};

	YouTubeVideo.prototype.pauseVideo = function () {
		this.player.pauseVideo();
		this.playerState = 'pause';
		this.$element.trigger('videoPaused');
	};

	YouTubeVideo.prototype.stopVideo = function () {
		this.init = false;
		this.playerState = 'stop';
		this.$element.trigger('videoStopped');
	};

	$.fn.youTubeVideo = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('youTubeVideo');

			if (!data) {
				data = new YouTubeVideo(this, typeof options === 'object' && options);
				$this.data('youTubeVideo', data);
			}

		});

	};

	$.fn.youTubeVideo.Constructor = YouTubeVideo;

}(window, document, jQuery));