(function scrollIndicator(window, $) {

	var $window = $(window),
			stInitial = 0;

	$window.on('scroll', function () {

		var st = $window.scrollTop();

		if(st > stInitial)
			$window.trigger('scrollDown');
		else
			$window.trigger('scrollUp');

		stInitial = st;

	});

}(window, jQuery));