(function gallery(global, document, $) {

	function Gallery(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	Gallery.prototype.defaults = {

		structure: {
			switchContainer: '.gallery__switch',
			switchTab: '.switch__tab',
			tabsContainer: '.gallery__tabs',
			tab: '.gallery__tab'
		},
		dataTabKey: 'tab',
		autoData: true,
		disabledClass: '.disabled',
		autoPlay: false,
		autoPlayInterval: 4000,
		switchOnClick: true,
		switchOnHover: false

	};

	function init() {

		$.proxy(setStructure, this)();

		if(this.options.autoData)
			$.proxy(setData, this)();

		this.currentTabNum = $.proxy(getCurrentTabNum, this)();
		this.tabCount = this.$tabs.length;

		if (this.options.autoPlay)
			$.proxy(startAnimation, this)();

		if (this.options.switchOnClick)
			$.proxy(setSwitchTabHandler, this, 'click')();

		if (this.options.switchOnHover)
			$.proxy(setSwitchTabHandler, this, 'mouseenter')();

	}

	function setStructure() {

		var self = this,
				$gallery = self.$element,
				structure = self.options.structure;

		self.$switch = $gallery.children(structure.switchContainer);
		self.$switchTabs = self.$switch.find(structure.switchTab);
		self.$tabsContainer = $gallery.children(structure.tabsContainer);
		self.$tabs = self.$tabsContainer.children(structure.tab);

	}

	function setData() {

		var self = this,
				key = self.options.dataTabKey;

		self.$switchTabs.each(function (ind, el) {
			var $el = $(el);
			$el.data(key, ind + 1);
		});

		self.$tabs.each(function (ind, el) {
			var $el = $(el);
			$el.data(key, ind + 1);
		});
	}

	function switchTab(activeTab) {

		var self = this,
				nextTabNum = self.currentTabNum < self.tabCount ? self.currentTabNum + 1 : 1,
				tab = activeTab || nextTabNum;

		self.currentTabNum = tab;

		self.$switchTabs.each(function (ind, el) {
			$.proxy(activateTab, self, $(el), tab)();
		});

		self.$tabs.each(function (ind, el) {
			$.proxy(activateTab, self, $(el), tab)();
		});

	}

	function activateTab($tab, activeTab) {

		var active = window.classes.active,
				key = this.options.dataTabKey;

		if ($tab.data(key) === activeTab)
			$tab.addClass(active);
		else
			$tab.removeClass(active);
	}

	function getTab(tab) {

		var self = this,
				$tab = undefined;
		self.$tabs.each(function (ind, el) {
			var $el = $(el);
			if ($el.data(self.options.dataTabKey) === tab) {
				$tab = $el;
				return false;
			}
		});
		return $tab;

	}

	function getCurrentTabNum() {
		var self = this,
				tabNum = undefined;
		self.$tabs.each(function (ind, el) {
			var $el = $(el);
			if ($el.hasClass(window.classes.active)) {
				tabNum = $el.data(self.options.dataTabKey);
				return false;
			}
		});
		return tabNum;
	}

	function setSwitchTabHandler(event) {

		var self = this,
				switchTabClass = self.options.structure.switchTab,
				disabled = self.options.disabled;

		self.$element.on(event, switchTabClass + ':not(' + disabled + ')', function () {

			if (self.animationStarted)
				$.proxy(stopAnimation, self)();

			$.proxy(switchTabHandler, this, self)();

		}).on('mouseleave', switchTabClass + ':not(' + disabled + ')', function () {
			if (self.options.autoPlay)
				$.proxy(startAnimation, self)();
		});
	}

	function switchTabHandler(mainContext) {

		var $tab = $(this),
				tab = $tab.data(mainContext.options.dataTabKey);

		$.proxy(switchTab, mainContext, tab)();
		mainContext.$element.trigger('tabChanged', [tab, $.proxy(getTab, mainContext, tab)()]);
	}

	function startAnimation() {
		var self = this;
		if (!self.animationStarted) {
			self.intervalId = setInterval($.proxy(switchTab, self), self.options.autoPlayInterval);
			self.animationStarted = true;
		}

	}

	function stopAnimation() {
		var self = this;
		clearInterval(self.intervalId);
		self.animationStarted = false;
	}

	Gallery.prototype.switchTab = function (tabNum) {

		$.proxy(switchTab, this, tabNum)();

	};

	$.fn.gallery = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('gallery');

			if (!data) {
				data = new Gallery(this, typeof options === 'object' && options);
				$this.data('gallery', data);
			}

		});

	};

	$.fn.gallery.Constructor = Gallery;

}(window, document, jQuery));