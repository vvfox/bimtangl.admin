(function videoCover(global, document, $) {

	function VideoCover(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();
	}

	VideoCover.prototype.defaults = {
		ratio: 16 / 9
	};

	function init() {
		var self = this;
		self.$parent = self.$element.parent();
		$(window).on('resize', function () {
			setTimeout(function () {
				self.resize();
			}, 300);
		}).resize();

	}

	function getWidth() {
		var videoContainerHeight = this.$parent.height(),
				videoContainerWidth = this.$parent.width(),
				ratio = this.options.ratio,
				parentRatio = videoContainerWidth / videoContainerHeight;
		return Math.ceil(parentRatio > ratio ? videoContainerWidth : videoContainerHeight * ratio);
	}

	function onResize() {
		var self = this,
				width = $.proxy(getWidth, self)(),
				height = width / self.options.ratio;

		self.$element.css({
			width: width,
			height: height
		});
	}

	VideoCover.prototype.resize = function () {
		$.proxy(onResize, this)();
	};


	$.fn.videoCover = function (options, ...params) {
		this.each(function () {
			let $this = $(this),
					data = $this.data('videoCover');
			if ((!options || typeof options === 'object') && !data) {
				data = new VideoCover(this, options);
				$this.data('videoCover', data);
			} else if (typeof options === 'string' && data && typeof data[options] === 'function') {
				data[options].apply(data, params);
			} else {
				throw 'videoCover: invalid call';
			}
		});
	};

	$.fn.videoCover.Constructor = VideoCover;

}(window, document, jQuery));