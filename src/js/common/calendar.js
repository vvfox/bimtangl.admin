(function calendar(global, document, $, pickmeup) {

	pickmeup.defaults.locales['ru'] = {
		days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
	};

	function Calendar(element, options) {

		this.options = $.extend(true, {}, this.defaults, options);
		this.element = element;
		this.$element = $(element);
		$.proxy(init, this)();

	}

	Calendar.prototype.defaults = {
		pickmeup: {
			hide_on_select: true,
			class_name: 'calendar',
			locale: 'ru',
			format: 'd.m.Y'
		},
		custom: {
			append: true,
			disablePastDays: false,
			minDay: getMinDay()
		}
	};

	Calendar.prototype.getDate = function (formatted) {

		var self = this,
				element = self.element;

		return pickmeup(element).get_date(formatted);

	};

	Calendar.prototype.setDate = function (date) {

		var self = this,
				element = self.element;

		pickmeup(element).set_date(date);

		return self;

	};

	Calendar.prototype.refresh = function (opts) {

		var self = this;
		self.options = $.extend(true, {}, self.options, opts);
		$.proxy(destroy, self)();
		$.proxy(init, self)();

	};

	function destroy() {

		pickmeup(this.element).destroy();

	}

	function init() {

		var self = this,
				element = self.element,
				$element = self.$element,
				calOptions = self.options.pickmeup,
				minDay = self.options.custom.minDay;

		if(self.options.custom.disablePastDays)
			calOptions = $.extend({}, self.options.pickmeup, {

				min: minDay,
				render: disablePastDays(minDay)

			});

		pickmeup(element, calOptions);

		if(self.options.custom.append)
			$element.parent().append($(element.__pickmeup.element));

		$element.on('pickmeup-change', function () {

			$element.trigger('change');

		});

	}

	function disablePastDays(day) {

		return function (date) {
			if (date < day) {
				return {disabled : true, class_name : 'date-in-past'};
			}
			return {};
		}

	}

	function getMinDay() {

		var now = new Date;
		return new Date(now.getFullYear(), now.getMonth(), now.getDate());

	}

	$.fn.calendar = function (options) {

		return this.each(function () {

			var $this = $(this),
					data = $this.data('calendar');

			if (!data) {
				data = new Calendar(this, typeof options === 'object' && options);
				$this.data('calendar', data);
			}

		});

	};

	$.fn.calendar.Constructor = Calendar;


}(window, document, jQuery, pickmeup));