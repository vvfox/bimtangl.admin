import VueRouter from 'vue-router';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';

import store from '@/store';

import OidcCallback from '@/page_scripts/OidcCallback';
import Auth from '@/page_scripts/auth';
import Main from '@/page_scripts/main';
import ProjectList from '@/page_scripts/project/';
import ProjectDetial from '@/page_scripts/project/detail';
import ModelsList from '@/page_scripts/models';
import ServiceList from  '@/page_scripts/services';
import Company from '@/page_scripts/company';
import CompanyMain  from '@/page_scripts/company/main';
import CompanyUnits from '@/page_scripts/company/units';
import CompanyAdmin from '@/page_scripts/company/admin';
import User from '@/page_scripts/users';
import UserConfig from '@/page_scripts/users/config';
import UserSecurity from '@/page_scripts/users/security';




export const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path:'/auth/', 
            component: Auth,
            name:'auth',
             meta: {
                isPublic: true
            }
        
        
        
        },
        {
            path:'/', 
            component: Main, 
            name:'main_page',
            // meta: {
            //     // isPublic: true
            //   }
        },
        {path:'/project/', component: ProjectList, name:'project_list',  props: true},
        {path:'/project/:id', component: ProjectDetial, name:'project_item',  props: true},
        {path:'/models', component: ModelsList, name:'models_list',  props: true},
        { path:'/services', component: ServiceList, name:'service_list',  props: true},
        {
            path:'/company', component: Company, name:'company',  props: true,
            children: [
                { path: 'main', component: CompanyMain, "name": 'company_main'},
                { path: 'units', component: CompanyUnits, "name": 'company_units'},
                { path: 'admin', component: CompanyAdmin, "name": 'company_admin'},
            ]
         },
        {
            path: '/user', component: User, name: 'user',
            children: [
                {path: 'config', component: UserConfig, name: 'user_config'},
                {path: 'security', component: UserSecurity, name: 'user_security'},
            ]
        },

        {
            path: '/oidc-callback', // Needs to match redirectUri (redirect_uri if you use snake case) in you oidcSettings
            name: 'oidcCallback',
            component: OidcCallback
          }
    ]
});


         
router.beforeEach(vuexOidcCreateRouterMiddleware(store, 'oidcStore'))

