import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);
export const news = {
    namespaced: true,
    state: {

        items: [


            {

                title: 'Новый сервис от Tangl.cloud',
                pubDate:'Tue, 18 Sep 2018 10:48:13 +0300',
                link: 'http://www.example.com/item1-info-page.html',
                
                description: 'Смета — документ, в котором рассчитывают затраты на проект исходя из расходов: на работы, стройматериалы, хозяйственные нужды, приобретение комплектующих и прочее.',
                enclosure: {
                    url: '/static/img/news_item.jpg'
                }
            },
            {

                title: 'Обновления в сервисе от Tangl.cloud',
                pubDate:'Tue, 18 Sep 2018 10:48:13 +0300',
                link: 'http://www.example.com/item1-info-page.html',
                
                description: 'Смета — документ, в котором рассчитывают затраты на проект исходя из расходов: на работы, стройматериалы, хозяйственные нужды, приобретение комплектующих и прочее.',
                enclosure: {
                    url: '/static/img/news_item.jpg'
                }
            }
        ]


    }};