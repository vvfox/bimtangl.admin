


import Vuex from "vuex";
import Vue from "vue";
import axios from "axios";
Vue.use(Vuex);
export const service = {
    namespaced: true,
    state: {
        saas_info: {


            sum_disk_volume: 10,
            count_users_connect: 15,
            
            extra_disk_volume: 10,
            extra_count_users: 100, 

            cost_extra_disk_volume: 1000,
            cost_extra_user: 1000,
            

            used_disk_vloume: 10,
            used_count_user: 5


        },
        list: {
            'tangl_value': {
                id: 'tangl_value',
                name:'Value',
                description:'рассчет смет',
                picture:'',
                status:1,
                min_cost:500,
                current_rate: 'tangl_value_one',
                screenlist:[

                    '/static/img/screen_1.jpg',
                    '/static/img/screen_1.jpg',
                    '/static/img/screen_1.jpg',
                    '/static/img/screen_1.jpg',
                    '/static/img/screen_1.jpg',
                ],
                rates: {
                    tangl_value_one: {
                        id_rate:'tangl_value_one',
                        'name': 'Первый тариф',
                        count_users:10,
                        volume_hd:20,
                        cost:500,
                    },
                    tangl_value_two: {
                        id_rate:'tangl_value_two',
                        'name': 'Второй тариф',
                        count_users:20,
                        volume_hd:40,
                        cost:1000,
                    }
                }
            },
            'tangl_invest': {
                id: 'tangl_invest',
                name:'Invest',
                description:'инвестиционные риски',
                picture:'',
                status:0,
                min_cost:500,
                current_rate: 'tangl_value_one',
                //screenlist:[],
                rates: {
                    tangl_invest_one: {
                        id_rate:'tangl_invest_one',
                        'name': 'Первый тариф',
                        count_users:10,
                        volume_hd:20,
                        cost:500,
                    },
                    tangl_invest_two: {
                        id_rate:'tangl_invest_two',
                        'name': 'Второй тариф',
                        count_users:20,
                        volume_hd:40,
                        cost:1000,
                    }
                }
            }
        }
    },
};

