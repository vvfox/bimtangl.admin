
import { vuexOidcCreateStoreModule } from 'vuex-oidc'
import { oidcSettings } from '../oidc'


import { project } from "./project";
import { company } from './company';
import {model} from './model';
import {user} from './user';
import {service} from './service';
import {news} from './news';


import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);
export default new Vuex.Store({
    namespaced: true,
    modules: {
        oidcStore: vuexOidcCreateStoreModule(oidcSettings,  {
             namespaced: true ,
             dispatchEventsOnWindow: true

        }, 
        {
            userLoaded: (user) => console.log('OIDC user is loaded:', user),
            userUnloaded: () => console.log('OIDC user is unloaded'),
            accessTokenExpiring: () => console.log('Access token will expire'),
            accessTokenExpired: () => console.log('Access token did expire'),
            silentRenewError: () => console.log('OIDC user is unloaded'),
            userSignedOut: () => console.log('OIDC user is signed out'),
            oidcError: (payload) => console.log(`An error occured at ${payload.context}:`, payload.error),
            automaticSilentRenewError: (payload) => console.log('Automatic silent renew failed.', payload.error)
        }),
        project,
        company,
        model,
        user,
        service,
        news
    }
});