
import Vuex from "vuex";
import Vue from "vue";
import axios from "axios";
Vue.use(Vuex);
export const company = {
    namespaced: true,
    state: {
        data:{
            id_company: 'lhjgkhj-khvkjv0-khjvkj',
            name: 'ООО "Маяк"',
            inn: '098888666777',
            kpp: '0988886667',
            ogrn: '58765875867586758',
            ogrn_date: '1999.06.11',
            type: 'LEGAL',
            opf: 'ООО',
            short_name: 'ООО "Маяк"',
            'full_name': 'Общество с ограниченной отвественностью "Маяк"',
            okved: '52.06',
            address: '622000 Свердловская область, Екатеринбург, Маяковского 106, оф. 1',
            state:'ACTIVE',
            balance: 1000,
            currency: 'RUB',
            management:{
                f_name:'Константин',
                l_name:'Константинопольский',
                m_name:'Константинович',
                post:'директор',
                с: 'устава'
            }
        },
        users: {

            params:{
                current_page: 1,
                count_page:1,
                count_elements: 2,
                count_elements_page: 10
            },

            items:{
                'igfjhgfghc-hgvhjgcv-gcjhgc':
                {

                    id_user:'igfjhgfghc-hgvhjgcv-gcjhgc',
                    email: 'test@test.ru',
                    phone: '+7 (987) 77-77-777',
                    f_name: 'Андресис',
                    l_name: 'Лесвос',
                    point: ' Архитектор',
                    avatar: '/static/img/avatar.png',
                    active: 1
                },
                'igfjhgfghc-hgvhjgcv-gcjhgcQ':
                    {

                        id_user:'igfjhgfghc-hgvhjgcv-gcjhgcQ',
                        email: 'test@test.ru',
                        phone: '+7 (987) 77-77-777',
                        f_name: 'Константин',
                        l_name: 'Константинопольский',
                        point: ' Директор',
                        avatar: '',
                        active:0
                    }

            }


        }
    },
};