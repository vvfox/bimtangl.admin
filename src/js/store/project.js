
import Vuex from "vuex";
import Vue from "vue";
import axios from "axios";
Vue.use(Vuex);
export const project = {
    namespaced: true,
    state: {



        params:{
            current_page:1,
            count_page:1,
            count_elements: 2,
            count_elements_page: 10
        },
        items: {
            'hjgkjhgkj-kjhvkjh-jkhvkjhv':

                {
                    id_project:'hjgkjhgkj-kjhvkjh-jkhvkjhv',
                    name: "Наумов Парк",
                    img: '',
                    users: {
                        'hkjhgkjh0-khjvkjhv-bjhvkj':{

                            user_id: 'hkjhgkjh0-khjvkjhv-bjhvkj',
                            f_name: 'Виктор',
                            l_name: 'Андреев',
                            avatar: '',
                            permission: 'editor'
                        },
                        'hkjhgkjh0-khjvkjghv-bjhvkj':{

                            user_id: 'hkjhgkjh0-khjvkjghv-bjhvkj',
                            f_name: 'Константин',
                            l_name: 'Андреев',
                            avatar: '',
                            permission: 'watcher'
                        },
                    }


                },
            'hjgkjhgkj-kjhevkjh-jkhvkjhv':

                {
                    id_project:'hjgkjhgkj-kjhevkjh-jkhvkjhv',
                    name: "ЖК Март",
                    img: '/static/img/image_34.png',
                    users: {
                        'hkjhgkjh0-khjvkjhv-bjhvkj':{

                            user_id: 'hkjhgkjh0-khjvkjhv-bjhvkj',
                            f_name: 'Виктор',
                            l_name: 'Андреев',
                            avatar: '',
                            permission: 'editor'
                        },
                        'hkjhgkjh0-khjvkjghv-bjhvkj':{

                            user_id: 'hkjhgkjh0-khjvkjghv-bjhvkj',
                            f_name: 'Константин',
                            l_name: 'Андреев',
                            avatar: '',
                            permission: 'editor'
                        },

                        'igfjhgfghc-hgvhjgcv-gcjhgc':{
                        user_id:'igfjhgfghc-hgvhjgcv-gcjhgc',

                        f_name: 'Андресис',
                        l_name: 'Лесвос',

                        avatar: '/static/img/avatar.png',
                        permission: 'editor'
                    }

                    }


                }

        }
     },
};