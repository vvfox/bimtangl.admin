
export const oidcSettings = {
    // authority: 'https://your_oidc_authority',
    // clientId: 'your_client_id',
    // redirectUri: 'http://localhost:1337/oidc-callback',
    // responseType: 'id_token token',
    // scope: 'openid profile'



    authority: "https://auth.st.tangl.cloud/",
    client_id: "TanglAccount_Web",
    redirect_uri: "http://localhost:3000/oidc-callback",
    response_type: "code",
    scope:"openid email profile role IdentityServerApi",


    //silentRedirectUri: 'http://localhost:3000/auth',
    // automaticSilentRenew: true, // If true oidc-client will try to renew your token when it is about to expire
    // automaticSilentSignin: true // If true vuex-oidc will try to silently signin unauthenticated users on public routes. Defaults to true
  

  }
  